//
//  SPMSSidebarViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "RESideMenu.h"

@interface SPMSSidebarViewController : RESideMenu <RESideMenuDelegate>

@end
