//
//  SPMSLeftSidebarViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSLeftSidebarViewController.h"

#import <UIViewController+RESideMenu.h>
#import "SPMSLoginViewController.h"
#import "SPMSBrowseProductViewController.h"
#import "SPMSLeftSidebarTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "SPMSFacebookServiceObject.h"
#import "User.h"
#import <UIImageView+WebCache.h>

typedef void (^SPMSLogoutResponse)(void);

@interface SPMSLeftSidebarViewController ()

@end

@implementation SPMSLeftSidebarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadUserProfileView];
    [self configTableViewLayout];
    
    // Check whether the user is logged-in or not to display corresponding view
    RACSignal *loggedInSignal = [[NSUserDefaults standardUserDefaults] rac_channelTerminalForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
    
    [loggedInSignal subscribeNext:^(id value) {
        [self reloadUserProfileView];
    }];
}

#pragma mark - Check Login
- (void)reloadUserProfileView
{
    // Check for login
    BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
    if (!isUserLoggedIn) {
        // Hide User Avatar and Info, display "Ask to Login" Image
        [self configAskToLoginImageView];
    } else {
        [self configUserAvatarImageView];
    }
    [_tableView reloadData];
}

#pragma mark - Logout
- (void)logout:(SPMSLogoutResponse)completeBlock
{
    // Call SPMSFacebookService to check FBLogin
    if ([[SPMSFacebookServiceObject sharedFBServiceInstance] isLoggedIn]) {
        [[FBSession activeSession] closeAndClearTokenInformation];
    }
    // Truncate all User Information in "User" Entity in Core Date
    [User MR_truncateAll];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (completeBlock) {
        completeBlock();
    }
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0: {
            // Navigate to Browse Product Screen
            if ([self presentingViewController] != [SPMSBrowseProductViewController class]) {
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"browseProductController"]]
                                                            animated:YES];
            }
            [self.sideMenuViewController hideMenuViewController];
            break;
        }
        case 4: {
            // Logout
            [self logout:^{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
                [loginViewController setModalPresentationStyle:UIModalPresentationFullScreen];
                [self presentViewController:loginViewController animated:YES completion:nil];
                [self.sideMenuViewController hideMenuViewController];
            }];
            break;
        }
        default:
            break;
    }
}

#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    NSInteger numberOfRows = 5;
    
    BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
    if (!isUserLoggedIn) {
        numberOfRows = 4;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SidebarCell";
    SPMSLeftSidebarTableViewCell *cell = (SPMSLeftSidebarTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[SPMSLeftSidebarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSArray *titles = @[@"Browse Product", @"Promotion", @"Map", @"Settings", @"Log Out"];
    NSArray *images = @[@"LeftSidebarNav_BrowseProduct_ICO",
                        @"LeftSidebarNav_Promotion_ICO",
                        @"LeftSidebarNav_Map_ICO",
                        @"LeftSidebarNav_Setting_ICO",
                        @"IconEmpty"];
    cell.menuTitleLabel.text = titles[indexPath.row];
    cell.menuIconImageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
    cell.selectedBackgroundView = [[UIView alloc] init];
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout
{
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.opaque = NO;
    _tableView.backgroundView = nil;
    _tableView.bounces = NO;
    _tableView.scrollsToTop = NO;
}

#pragma mark - Layout
- (void)configUserAvatarImageView
{
    _userAvatarImageView.hidden = NO;
    _userFullNameLabel.hidden = NO;
    _viewAndEditProfileLabel.hidden = NO;
    _askToLoginImageView.hidden = YES;
    
    // Create Circular User Avatar Image View
    _userAvatarImageView.layer.cornerRadius = _userAvatarImageView.frame.size.width / 2;
    _userAvatarImageView.clipsToBounds = YES;
    
    // Add Border
    _userAvatarImageView.layer.borderWidth = 3.0f;
    _userAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // Set Data
    if ([[User MR_findAll] firstObject] != nil) {
        User *user = [[User MR_findAll] firstObject];
        _userFullNameLabel.text = user.fullName;
        [_userAvatarImageView setImageWithURL:[NSURL URLWithString:user.imageURL] placeholderImage:[UIImage imageNamed:@"Photo"]];
    }
}

- (void)configAskToLoginImageView
{
    _userAvatarImageView.hidden = YES;
    _userFullNameLabel.hidden = YES;
    _viewAndEditProfileLabel.hidden = YES;
    _askToLoginImageView.hidden = NO;
    
    [_askToLoginImageView setImage:[UIImage imageNamed:@"LeftSidebarNav_AskToLogin_IMG"]];
    _askToLoginImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(showLoginViewAnimated:)];
    [_askToLoginImageView addGestureRecognizer:tapGesture];
}

#pragma mark - Login
- (void)showLoginViewAnimated:(BOOL)animated
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginController"];
    [loginViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:loginViewController animated:animated completion:^{
        [self.sideMenuViewController hideMenuViewController];
    }];
}

#pragma mark - Other
- (void)dealloc {
}
@end
