//
//  SPMSFacebookServiceObject.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/9/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SPMSFacebookServiceObject : NSObject

+ (id)sharedFBServiceInstance;

- (BOOL)isLoggedIn;

@end
