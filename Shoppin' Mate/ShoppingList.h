//
//  ShoppingList.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product, User;

@interface ShoppingList : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * shoppingListId;
@property (nonatomic, retain) NSSet *productList;
@property (nonatomic, retain) User *user;
@end

@interface ShoppingList (CoreDataGeneratedAccessors)

- (void)addProductListObject:(Product *)value;
- (void)removeProductListObject:(Product *)value;
- (void)addProductList:(NSSet *)values;
- (void)removeProductList:(NSSet *)values;

@end
