//
//  SPMSLoginViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SPMSLoginViewController : UIViewController <UITextFieldDelegate, FBLoginViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *signInByEmailButton;
@property (strong, nonatomic) IBOutlet FBLoginView *signInViaFacebookView;

@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;

- (IBAction)forgetPasswordBTNTappedAction:(id)sender;
- (IBAction)signupBTNTappedAction:(id)sender;
- (IBAction)backgroundTappedAction:(id)sender;

@end
