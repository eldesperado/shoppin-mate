//
//  Shelf.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Shelf.h"
#import "Floor.h"
#import "Product.h"


@implementation Shelf

@dynamic height;
@dynamic shelfId;
@dynamic shelfName;
@dynamic width;
@dynamic xCoordinate;
@dynamic yCoordinate;
@dynamic floor;
@dynamic productList;

@end
