//
//  Floor.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Map, Shelf;

@interface Floor : NSManagedObject

@property (nonatomic, retain) NSString * floorId;
@property (nonatomic, retain) NSString * floorName;
@property (nonatomic, retain) Map *map;
@property (nonatomic, retain) NSSet *shelves;
@end

@interface Floor (CoreDataGeneratedAccessors)

- (void)addShelvesObject:(Shelf *)value;
- (void)removeShelvesObject:(Shelf *)value;
- (void)addShelves:(NSSet *)values;
- (void)removeShelves:(NSSet *)values;

@end
