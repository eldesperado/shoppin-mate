//
//  Promotion.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Promotion.h"
#import "Product.h"


@implementation Promotion

@dynamic content;
@dynamic endDate;
@dynamic promotionDescription;
@dynamic promotionId;
@dynamic promotionName;
@dynamic startDate;
@dynamic title;
@dynamic productList;

@end
