//
//  SPMSBrowseProductViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu.h>

@interface SPMSBrowseProductViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *productList;

@end
