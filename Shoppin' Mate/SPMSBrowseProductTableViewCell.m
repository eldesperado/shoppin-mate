//
//  HomeFeedTableViewCell.m
//  Ohyeap
//
//  Created by El Desperado on 7/15/13.
//  Copyright (c) 2013 El Desperado. All rights reserved.
//

#import "SPMSBrowseProductTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

#import <UIImageView+WebCache.h>

@implementation SPMSBrowseProductTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setProduct:(Product *)product
{
    _productNameLabel.text = product.productName;
    _productDescLabel.text = product.productDescription;
    _productPriceLabel.text = [NSString stringWithFormat:@"%@k", product.price];
    [_productImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", product.imageURL]] placeholderImage:[UIImage imageNamed:@"Photo"]];
    [_productImageView setClipsToBounds:YES];
}
@end
