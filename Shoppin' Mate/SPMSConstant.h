//
//  SPMSConstant.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOGGEDIN_NOTIFICATION @"loggedIn"
#define LOGGOUT_NOTIFICATION @"logout"
#define NSUSERDEFAULTS_LOGGEDIN_KEY @"userLoggedIn"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define USER_GENDER_MALE 0
#define USER_GENDER_FEMALE 1
#define FB_API_READ_PERMISSIONS_NEEDED @[@"public_profile", @"email", @"user_birthday"]

@interface SPMSConstant : NSObject

@end
