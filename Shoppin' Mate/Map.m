//
//  Map.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Map.h"
#import "Floor.h"


@implementation Map

@dynamic createdDate;
@dynamic mapData;
@dynamic mapId;
@dynamic mapName;
@dynamic floor;

@end
