//
//  Product.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Product.h"
#import "CategoryType.h"
#import "Promotion.h"
#import "Shelf.h"
#import "ShoppingList.h"


@implementation Product

@dynamic discount;
@dynamic imageURL;
@dynamic price;
@dynamic productDescription;
@dynamic productId;
@dynamic productName;
@dynamic createdDate;
@dynamic category;
@dynamic promotion;
@dynamic shelf;
@dynamic shoppingLists;

@end
