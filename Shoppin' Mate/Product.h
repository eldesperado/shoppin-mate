//
//  Product.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CategoryType, Promotion, Shelf, ShoppingList;

@interface Product : NSManagedObject

@property (nonatomic, retain) NSNumber * discount;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * productDescription;
@property (nonatomic, retain) NSString * productId;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) CategoryType *category;
@property (nonatomic, retain) Promotion *promotion;
@property (nonatomic, retain) Shelf *shelf;
@property (nonatomic, retain) ShoppingList *shoppingLists;

@end
