//
//  CategoryType.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

@interface CategoryType : NSManagedObject

@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSSet *productList;
@end

@interface CategoryType (CoreDataGeneratedAccessors)

- (void)addProductListObject:(Product *)value;
- (void)removeProductListObject:(Product *)value;
- (void)addProductList:(NSSet *)values;
- (void)removeProductList:(NSSet *)values;

@end
