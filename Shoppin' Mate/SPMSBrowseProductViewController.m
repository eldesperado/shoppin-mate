//
//  SPMSBrowseProductViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSBrowseProductViewController.h"

#import "Product.h"
#import "SPMSBrowseProductTableViewCell.h"
#import <SVPullToRefresh.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <POP/POP.h>

@interface SPMSBrowseProductViewController ()
{
    BOOL _isNavigationItemAnimate;
    UISearchBar *_searchBar;
}
@end

@implementation SPMSBrowseProductViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    // Config Sidebar
    [self configNavigationBar];
    // Config TableView layout
    [self configTableViewLayout];
    
    // Remove all data in Entity to refresh DEMO data
    [Product MR_truncateAll];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    // Reload Data
    [self reloadFetchedResults:nil];
    
    // Config PullToRefresh
    [self configPullToRefresh];
    
    _searchBar = [[UISearchBar alloc] initWithFrame:self.tableView.bounds];
    
    self.navigationItem.titleView = _searchBar;
    
    _searchBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [_tableView triggerPullToRefresh];
}

#pragma mark - Generate DEMO data
- (void)generateDEMODataWithDate:(NSDate *)date
{
    NSArray *productNames = @[@"Cơm Gà Hải Nam",
                              @"Yolo Beer",
                              @"Icrazy Take Away",
                              @"Cơm tấm Ngon",
                              @"Quán Mến Lộc"];
    NSArray *productDescriptions = @[@"Cơm gà ở đây làm theo kiểu Singapo vị rất đậm đà và ngon.",
                                     @"Quán có cái tên khá lạ Yolo beer,nằm ngay vị trí 2 mặt tiền, không gian mở cả 2 phía.",
                                     @" Quán mới khai trương, không gian cũng khá thoáng mát với những chiếc bàn ghế gỗ sắp đặt ở trong quán và cả ngoài quán.",
                                     @"Quán này mới mở nên vẫn hơi còn vắng khách. Tuy nhiên vì là không gian mở nên khá là thoáng.",
                                     @"Vừa vô quán là mình đã cảm thấy thích cái không gian của quán rồi, một không gian rất là yên tĩnh và ấm cúng."];
    NSArray *images= @[@"https://ohyeap.com/storage/large_1401609230759_3e07701b8bbe7d25fe67af.jpg",
                       @"https://ohyeap.com/storage/large_1399954742587_cee91c53ce9a14ac2ef2ce.jpg",
                       @"https://ohyeap.com/storage/large_1401011818015_431b9aa9365d5e193502a8.jpg",
                       @"https://ohyeap.com/storage/large_1401427395312_db80112d69ecd2d0ada6de.jpg",
                       @"https://ohyeap.com/storage/large_1401813285760_0d999abae6effb2e84b53f.jpg"];
    
    for (int i = 0; i < 4; i++) {
        Product *product = [Product MR_createEntity];
        product.productId = [NSString stringWithFormat:@"%i",i];
        product.productName = [productNames objectAtIndex:i];
        product.productDescription = [productDescriptions objectAtIndex:i];
        product.price = [NSNumber numberWithInt:i * 10];
        product.createdDate = date == nil?[NSDate date]:date;
        product.discount = [NSNumber numberWithInt:i * 5];
        product.imageURL = [images objectAtIndex:i];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}

#pragma mark - ReloadFetchedResults

// Because the app delegate now loads the NSPersistentStore into the NSPersistentStoreCoordinator asynchronously
// we will see the NSManagedObjectContext set up before any persistent stores are registered
// we will need to fetch again after the persistent store is loaded
- (void)reloadFetchedResults:(NSNotification*)note {
    
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
    
    if (note) {
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    
	if (count == 0) {
		count = 1;
	}
	
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
	
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    static NSString *CellIdentifier = @"BrowseProductTableViewCell";
    SPMSBrowseProductTableViewCell *cell = (SPMSBrowseProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(SPMSBrowseProductTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell
	Product *product = (Product *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.product = product;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 270.0f;
}

#pragma mark - FetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    // Set up the fetched results controller if needed.
    if (_fetchedResultsController == nil) {
        
        NSArray *records = [Product MR_findAllSortedBy:@"createdDate" ascending:NO];
        if ([records count] <= 0) {
            [self generateDEMODataWithDate:nil];
        }
        self.fetchedResultsController = [Product MR_fetchAllSortedBy:@"createdDate" ascending:NO withPredicate:nil groupBy:nil delegate:self];
    }
	
	return _fetchedResultsController;
}


/**
 Delegate methods of NSFetchedResultsController to respond to additions, removals and so on.
 */

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	UITableView *tableView = self.tableView;
	
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self configureCell:(SPMSBrowseProductTableViewCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}

#pragma mark - PullToRefresh
- (void)configPullToRefresh
{
    __weak SPMSBrowseProductViewController *weakSelf = self;
    
    // Setup pull-to-refresh
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    
    [self.tableView.pullToRefreshView setTitle:@"Getting New Products" forState:SVPullToRefreshStateLoading];
    
    // Setup infinite scrolling
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
}

- (void)insertRowAtTop
{
    __weak SPMSBrowseProductViewController *weakSelf = self;
    
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        Product *product = [Product MR_createEntity];
        float idx = CFAbsoluteTimeGetCurrent() / 10000;
        product.productId = [NSString stringWithFormat:@"%f", idx];
        product.productName = [NSString stringWithFormat:@"Product #%f", idx];
        product.productDescription = [NSString stringWithFormat:@"Desc of Product #%f", idx];
        product.price = [NSNumber numberWithInt:idx / 10];
        product.discount = [NSNumber numberWithInt:idx / 10];
        product.createdDate = [NSDate date];
        product.imageURL = [NSString stringWithFormat:@"%@", @"https://ohyeap.com/storage/large_1401011818015_431b9aa9365d5e193502a8.jpg"];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            [_tableView reloadData];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }];
    });
}

- (void)insertRowAtBottom {
    __weak SPMSBrowseProductViewController *weakSelf = self;
    
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow: -(60.0f*60.0f*24.0f)];
        [self generateDEMODataWithDate:yesterday];
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
    });
}

#pragma mark - TableView Layout Configuration
- (void)configTableViewLayout
{
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.opaque = NO;
    _tableView.backgroundView = nil;
}

#pragma mark - Sidebar Configuration
- (void)configNavigationBar
{
    // "Show Menu" Button
    UIButton *showMenuBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    showMenuBTN.frame = CGRectMake(0, 0, 30, 30);
    [showMenuBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowMenu_ICO"] forState:UIControlStateNormal];
    [showMenuBTN addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *showMenuBTNItem = [[UIBarButtonItem alloc] initWithCustomView:showMenuBTN];
    // "Search Product" Button
    UIButton *searchBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBTN.frame = CGRectMake(5, 0, 30, 30);
    [searchBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_Search_ICO"] forState:UIControlStateNormal];
    [searchBTN addTarget:self action:@selector(showSearchBar:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchBTNItem = [[UIBarButtonItem alloc] initWithCustomView:searchBTN];
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:showMenuBTNItem, searchBTNItem, nil]];
    // "Show Shopping List" Button
    UIButton *showShoppingListBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    showShoppingListBTN.frame = CGRectMake(0, 0, 30, 30);
    [showShoppingListBTN setImage:[UIImage imageNamed:@"BrowseProduct_Nav_ShowShoppingList_ICO"] forState:UIControlStateNormal];
    [showShoppingListBTN addTarget:self action:@selector(presentRightMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *showShoppingListBTNItem = [[UIBarButtonItem alloc] initWithCustomView:showShoppingListBTN];
    self.navigationItem.rightBarButtonItem = showShoppingListBTNItem;
    
    
}

#pragma mark - Action
- (void)showSearchBar:(id)sender
{
    double rightNavItemPosDelta;
    
    if (CGRectGetMaxX(self.navigationItem.rightBarButtonItem.customView.frame) < self.view.frame.size.width) {
        rightNavItemPosDelta = 20.0f;
    } else {
        rightNavItemPosDelta = - 20.0f;
    }
    
    POPBasicAnimation *moveRightNavItemAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPosition];
    moveRightNavItemAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(CGRectGetMaxX(self.navigationController.navigationBar.frame) + rightNavItemPosDelta, self.navigationController.navigationBar.frame.origin.y)];
    moveRightNavItemAnimation.duration = 0.3;
    
    [self.navigationItem.rightBarButtonItem.customView pop_addAnimation:moveRightNavItemAnimation forKey:@"moveRightNavItemAnimation"];
    
    double leftNavItemPosDelta;
    
    UIView *searchButtonView = [[self.navigationItem.leftBarButtonItems objectAtIndex:1] customView];
    
    if (searchButtonView.frame.origin.x < self.navigationController.navigationBar.frame.size.width / 2) {
        leftNavItemPosDelta = 230.0f;
    } else {
        leftNavItemPosDelta = - 230.0f;
    }
    
    POPBasicAnimation *moveLeftNavItemAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionX];
    moveLeftNavItemAnimation.toValue = @(CGRectGetMidX(searchButtonView.frame) + leftNavItemPosDelta);
    moveLeftNavItemAnimation.duration = 0.3;
    
    [[[self.navigationItem.leftBarButtonItems objectAtIndex:1] customView] pop_addAnimation:moveLeftNavItemAnimation forKey:@"moveLeftNavItemAnimation"];
    
    
    UIView *menuButtonView = [[self.navigationItem.leftBarButtonItems objectAtIndex:0] customView];
    
    menuButtonView.hidden = menuButtonView.hidden == YES ? NO : YES;
    
    _searchBar.hidden = _searchBar.hidden == YES ? NO : YES;
}

#pragma mark - Others
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// clean up our new observers
- (void)viewDidUnload {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
