//
//  SPMSDetailViewController.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/3/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSDetailProductViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
