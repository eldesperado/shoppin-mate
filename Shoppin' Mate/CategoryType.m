//
//  CategoryType.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "CategoryType.h"
#import "Product.h"


@implementation CategoryType

@dynamic categoryDescription;
@dynamic categoryId;
@dynamic categoryName;
@dynamic productList;

@end
