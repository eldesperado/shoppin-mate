//
//  UIColor+CustomColor.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/7/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)alohaRedColor
{
    return [UIColor colorWithRed:236.0f green:93.0f blue:98.0f alpha:1];
}

@end
