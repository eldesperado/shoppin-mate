//
//  Shelf.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Floor, Product;

@interface Shelf : NSManagedObject

@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSString * shelfId;
@property (nonatomic, retain) NSString * shelfName;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSString * xCoordinate;
@property (nonatomic, retain) NSString * yCoordinate;
@property (nonatomic, retain) Floor *floor;
@property (nonatomic, retain) NSSet *productList;
@end

@interface Shelf (CoreDataGeneratedAccessors)

- (void)addProductListObject:(Product *)value;
- (void)removeProductListObject:(Product *)value;
- (void)addProductList:(NSSet *)values;
- (void)removeProductList:(NSSet *)values;

@end
