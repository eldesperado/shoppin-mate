//
//  User.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ShoppingList;

@interface User : NSManagedObject

@property (nonatomic, retain) NSDate * dateOfBirth;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSSet *shoppingLists;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addShoppingListsObject:(ShoppingList *)value;
- (void)removeShoppingListsObject:(ShoppingList *)value;
- (void)addShoppingLists:(NSSet *)values;
- (void)removeShoppingLists:(NSSet *)values;

@end
