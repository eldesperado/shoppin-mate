//
//  ShoppingList.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "ShoppingList.h"
#import "Product.h"
#import "User.h"


@implementation ShoppingList

@dynamic createdDate;
@dynamic shoppingListId;
@dynamic productList;
@dynamic user;

@end
