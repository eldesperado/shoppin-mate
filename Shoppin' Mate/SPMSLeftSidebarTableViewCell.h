//
//  SPMSLeftSidebarTableViewCell.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMSLeftSidebarTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuIconImageView;
@property (strong, nonatomic) IBOutlet UILabel *menuTitleLabel;

@end
