//
//  Promotion.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

@interface Promotion : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * promotionDescription;
@property (nonatomic, retain) NSString * promotionId;
@property (nonatomic, retain) NSString * promotionName;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *productList;
@end

@interface Promotion (CoreDataGeneratedAccessors)

- (void)addProductListObject:(Product *)value;
- (void)removeProductListObject:(Product *)value;
- (void)addProductList:(NSSet *)values;
- (void)removeProductList:(NSSet *)values;

@end
