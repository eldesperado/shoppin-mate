//
//  Floor.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Floor.h"
#import "Map.h"
#import "Shelf.h"


@implementation Floor

@dynamic floorId;
@dynamic floorName;
@dynamic map;
@dynamic shelves;

@end
