//
//  Map.h
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Floor;

@interface Map : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * mapData;
@property (nonatomic, retain) NSString * mapId;
@property (nonatomic, retain) NSString * mapName;
@property (nonatomic, retain) Floor *floor;

@end
