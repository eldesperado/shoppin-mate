//
//  SPMSLoginViewController.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/5/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "SPMSLoginViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import "User.h"

typedef void (^SPMSSignInResponse)(BOOL);

#define EMAIL @"trungpnnse90009@fpt.edu.vn"
#define PASSWORD @"aloha"

@interface SPMSLoginViewController ()

@end

@implementation SPMSLoginViewController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Config Signal for all Button
    [self configButtonAction];
    // Check state of Email & Password input to validate
    [self checkForValidLoginInput];
    // Config FBLogin
    [self configFacebookLogin];
}

#pragma mark - Input Validation
/**
 *  Check User Input when Logging-in to validate
 */
- (void)checkForValidLoginInput
{
    RACSignal *validEmailSignal = [self.emailTextField.rac_textSignal map:^id(NSString *text) {
        return @([self isValidEmail:text]);
    }];
    RACSignal *validPasswordSignal = [self.passwordTextField.rac_textSignal map:^id(NSString *text) {
        return @([self isValidPassword:text]);
    }];
    
    // If String is not valid, text color is red, otherwise is green
    RAC(self.emailTextField, textColor) = [validEmailSignal map:^id(NSNumber *isValidEmail) {
        return [isValidEmail boolValue] ? [UIColor greenColor] : [UIColor redColor];
    }];
    
    // If String is not valid, text color is red, otherwise is green
    RAC(self.passwordTextField, textColor) = [validPasswordSignal map:^id(NSNumber *isValidPass) {
        return [isValidPass boolValue] ? [UIColor greenColor] : [UIColor redColor];
    }];
    
    // Signal to detect to enable/disable "Login" button whether the input is valid or not
    RACSignal *isActiveLoginBTNSignal = [RACSignal combineLatest:@[validEmailSignal, validPasswordSignal] reduce:^id(NSNumber *isValidEmail, NSNumber *isValidPass) {
        return @([isValidEmail boolValue] && [isValidPass boolValue]);
    }];
    // Enable/Disable "Login" button whether the input is valid or not
    [isActiveLoginBTNSignal subscribeNext:^(NSNumber *isValidLogin) {
        self.signInByEmailButton.enabled = [isValidLogin boolValue];
    }];
}

/**
 *  Validate User's Email String
 *
 *  @param emailString User's Email
 *
 *  @return BOOL
 */
- (BOOL)isValidEmail:(NSString*)emailString {
    
    if([emailString length] == 0){
        return NO;
    }
    
    NSString *regExPattern = REGEX_EMAIL;
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}
/**
 *  Validate User Account's Password String
 *
 *  @param passwordString User Account's Password
 *
 *  @return BOOL
 */
- (BOOL)isValidPassword:(NSString*)passwordString {
    if([passwordString length] <= 3){
        return NO;
    } else return YES;
}

#pragma mark - Config Facebook Login
/**
 *  Config FBLoginView
 */
- (void)configFacebookLogin
{
    _signInViaFacebookView.readPermissions = FB_API_READ_PERMISSIONS_NEEDED;
    _signInViaFacebookView.delegate = self;
}

#pragma mark - FBLoginViewDelegate
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSLog(@"[FBLogin Error] %@", [error localizedDescription]);
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    _signInViaFacebookView.hidden = YES;
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    NSString *email = [user objectForKey:@"email"];
    // Check whether an account is existed or not
    [self signInViaFBWithEmail:email complete:^(BOOL success) {
        NSString *userId = user.objectID;
        NSString *userFullname = user.name;
        NSString *dateOfBirth = user.birthday;
        NSString *gender = [user objectForKey:@"gender"];
        NSString *imageURL=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?redirect=true", user.username];
        // Save to Core Data
        [self getAndSaveUserProfileWithEmail:email userId:userId fullname:userFullname dob:dateOfBirth gender:gender avatarImageURL:imageURL];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark - Authentication
- (void)signInViaFBWithEmail:(NSString *)emailString complete:(SPMSSignInResponse)completeBlock
{
    BOOL isExistedUserAccount = [self isExistedUserAccountWithEmail:emailString];
    
    if (isExistedUserAccount) {
        // Save to NSUserDefaults
        if (completeBlock) {
            completeBlock(isExistedUserAccount);
        }
    }
    

}

- (BOOL)isExistedUserAccountWithEmail:(NSString *)emailString
{
    /*
     Send request to Server to check whether any User Account whose email is equal with "emailString" is existed or not
     */
    
    return YES;
}

/**
 *  Creates a signal that signs in with the current email and password
 *
 *  @return a Signal
 */
- (RACSignal *)signInViaEmailSignal
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self signInWithEmail:_emailTextField.text password:_passwordTextField.text complete:^(BOOL success) {
            [subscriber sendNext:@(success)];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

/**
 *  Sign-in with Email
 *
 *  @param emailString    User's Email
 *  @param passwordString User Account's Password
 *  @param completeBlock  Complete Block
 */
- (void)signInWithEmail:(NSString *)emailString password:(NSString *)passwordString complete:(SPMSSignInResponse)completeBlock
{
    BOOL isUserAuthenticated = [self isValidAutSignInWithEmail:emailString password:passwordString];
    
    if (isUserAuthenticated) {
        NSString *userId = @"123456789";
        NSString *userFullname = @"Nhật Trung";
        NSString *dateOfBirth = @"15/03/1992";
        NSString *gender = @"male";
        NSString *imageURL = @"https://scontent-b-hkg.xx.fbcdn.net/hphotos-prn2/t1.0-9/548379_436821256364837_1359851788_n.jpg";
        
        [self getAndSaveUserProfileWithEmail:emailString userId:userId fullname:userFullname dob:dateOfBirth gender:gender avatarImageURL:imageURL];
        // Save to NSUserDefaults
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (completeBlock) {
        completeBlock(isUserAuthenticated);
    }
}
/**
 *  Send Login Authentication Resquest to Server
 *
 *  @param emailString    User's Email
 *  @param passwordString User Account's Password
 *
 *  @return BOOL
 */
- (BOOL)isValidAutSignInWithEmail:(NSString *)emailString password:(NSString *)passwordString
{
    BOOL isUserAuthenticated = NO;
    
    /*
     Send Authentication request to server
    */
    
    // Demo validation
    if ([emailString isEqualToString:EMAIL] && [passwordString isEqualToString:PASSWORD]) {
        isUserAuthenticated = YES;
    }
    
    return isUserAuthenticated;
}

#pragma mark - Save User Profile to Core Data
- (void)getAndSaveUserProfileWithEmail:(NSString *)email userId:(NSString *)userId fullname:(NSString *)fullname dob:(NSString *)dob gender:(NSString *)gender avatarImageURL:(NSString *)imageURL
{
    /*
     Get User Profile From Server
     */
    User *user = [User MR_createEntity];
    user.userId = userId;
    user.email = email;
    user.fullName = fullname;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    user.dateOfBirth = [dateFormatter dateFromString:dob];;
    user.gender = [gender isEqualToString:@"male"] ? [NSNumber numberWithInt:USER_GENDER_MALE] : [NSNumber numberWithInt:USER_GENDER_FEMALE];
    user.imageURL = imageURL;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

#pragma mark - Action
/**
 *  Configurate Buttons' Action
 */
- (void)configButtonAction
{
    // Sign-in by Email
    [[[[_signInByEmailButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:^(id x) {
        _signInByEmailButton.enabled = NO;
      }]
      flattenMap:^(id x) {
        /*
         Map the button touch event to a sign-in signal, but also flattens it by sending the events from the inner signal to the outer signal
         */
        return [self signInViaEmailSignal];
    }]
     subscribeNext:^(NSNumber *signedIn) {
         _signInByEmailButton.enabled = YES;
         BOOL isSucceedLogin = [signedIn boolValue];
         // Dimiss Login View if logged-in successfully
         if (isSucceedLogin) {
             [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
         }
    }];
}

- (IBAction)forgetPasswordBTNTappedAction:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signupBTNTappedAction:(id)sender
{
    
}

- (IBAction)backgroundTappedAction:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder to navigate through TextFields
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
        [self signInWithEmail:_emailTextField.text password:_passwordTextField.text complete:^(BOOL success) {
            if (success) {
                [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    }
    return NO;
}

#pragma mark - Others
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
