//
//  User.m
//  Shoppin' Mate
//
//  Created by El Desperado on 6/4/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "User.h"
#import "ShoppingList.h"


@implementation User

@dynamic dateOfBirth;
@dynamic email;
@dynamic fullName;
@dynamic gender;
@dynamic imageURL;
@dynamic password;
@dynamic phoneNumber;
@dynamic userId;
@dynamic shoppingLists;

@end
