![AppIcon76x76@2x.png](https://bitbucket.org/repo/7nExrz/images/3414945409-AppIcon76x76%402x.png)

## Mobile Client Application of Shoppin' Mate System using Indoor Positioning System (IPS) ##

**Shoppin’ Mate System** is a web and mobile application developed for supermarket’s manager and supermarket’s consumers. The core of SPMS is the Indoor Positioning System, which uses iBeacon (Bluetooth Low Energy device) deployed inside buildings to enable highly accurate indoor positioning, this includes 3 main components: SPM Website, SPM Client Application and SPM Map Editor Application.

I am so proud of Shoppin' Mate System because the primary objective of SPMS is to serve supermarket’s consumers, bring out to them the best shopping experience. To do that, SPMS consists of many features that not only solve the existing matters of consumers (such as forgetting things to buy, having difficulties to find out certain items, losing much time go around the supermarket because of not being able to determine the shortest path to buy whole of shopping list…) but also offer a lot of useful utilities to meet consumers’ taste and interests while going shopping.

- **SPM Map Editor** is a mobile application running on iPad in order to establish the map of all floors in supermarket, each map for each floor plan.

- **SPM Website** is developed for the purpose of supporting a tool to manage consumer’s information and get useful reports of statistics from using SPM Client Application of consumers.

- **SPM Client Application** is a mobile application running on iPhone. This is the application intended for supermarket’s consumers. All utilities or consumer services is provided to consumers through this application.

**- Technologies & frameworks we used:**
+ Web: ASP.NET MVC 5, ASP.NET Web API 2, ASP.NET Identity 2, Entity Framework, Restful APIs, OAuth 2.0
+ Mobile: CoreData, AFNetworking, ReactiveCocoa, FMDB, QuartzCore, CATiledLayer, MagicalRecord, CoreLocation, CoreBluetooth
+ Hardware: iBeacons

**Algorithms we used:**
- Dijkstra’s Algorithm, Nearest Neighborhood Algorithm in order to calculated the shortest path which throughs all desired iBeacons' position.
- Smith-Waterman Algorithm to implemented a smart and fault-tolerant search which could accept unexpected inputs from users and provide related and suggested results.